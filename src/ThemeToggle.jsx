import { useContext } from "react";
import ThemeContext from "./ThemeContext";

const ThemeToggle = () => {
  const [theme, setTheme] = useContext(ThemeContext);
  return (
    <button
      className="toggle-theme"
      onClick={() =>
        theme.darkMode
          ? setTheme({ darkMode: false })
          : setTheme({ darkMode: true })
      }
    >
      Switch to {theme.darkMode ? "Light Mode" : "Dark Mode"}
    </button>
  );
};

export default ThemeToggle;
