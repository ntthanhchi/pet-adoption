async function fetchSearch({ queryKey }) {
  const pageNumber = queryKey[1];
  let { animal, location, breed } = queryKey[2] ? queryKey[2] : "";

  const res = await fetch(
    `https://pets-v2.dev-apis.com/pets?animal=${animal}&location=${location}&breed=${breed}&page=${pageNumber}`
  );

  if (!res.ok) {
    throw new Error(`pet search not ok ${animal}, ${location}, ${breed}`);
  }

  return res.json();
}

export default fetchSearch;
