/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
const pageSize = 10;

const Pagination = (props) => {
  const { currPage, totalItems, handleOnClick } = props;
  const totalPage = Math.ceil(totalItems / pageSize);

  if (totalItems <= pageSize) {
    return;
  }

  const listPage = [];
  for (let i = 0; i < totalPage; i++) {
    listPage.push(
      <li
        key={`page-${i + 1}`}
        className={currPage - 1 === i ? "active" : ""}
        onClick={() => handleOnClick(i + 1)}
      >
        {i + 1}
      </li>
    );
  }

  return (
    <div className="pagination">
      <ul>
        <li
          key="pre"
          className={currPage === 1 ? "disabled" : ""}
          onClick={() => handleOnClick(currPage - 1)}
        >
          previous
        </li>
        {listPage}
        <li
          key="next"
          className={currPage === totalPage ? "disabled" : ""}
          onClick={() => handleOnClick(currPage + 1)}
        >
          next
        </li>
      </ul>
    </div>
  );
};

export default Pagination;
