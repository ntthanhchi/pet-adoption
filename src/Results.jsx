import Pagination from "./Pagination";
import Pet from "./Pet";

const Results = (props) => {
  const { pets } = props;
  return (
    <div className="search">
      {!pets.length ? (
        <h1>No Pets Found</h1>
      ) : (
        <div>
          {pets.map((pet) => (
            <Pet
              key={pet.id}
              id={pet.id}
              name={pet.name}
              animal={pet.animal}
              breed={pet.breed}
              images={pet.images}
              location={`${pet.city}, ${pet.state}`}
            />
          ))}
          <Pagination {...props} />
        </div>
      )}
    </div>
  );
};

export default Results;
