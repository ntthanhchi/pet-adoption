import { useState } from "react";
import ThemeContext from "./ThemeContext";

const ThemeProvider = ({ children }) => {
  const themeToggle = useState({ darkMode: false });

  return (
    <ThemeContext.Provider value={themeToggle}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeProvider;
