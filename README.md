PET ADOPTION WEB
===================

Web use ReactJS, Vite

## 1. Installation
```bash
npm i
```

## 2. Tasks
- `npm run dev`: Run Project
- `npm run build`: Build Project (min)
- `npm run test`: Run test case


###### DEMO ######
https://web-pet-adoption.netlify.app/
